# webpack-com-php

1. Iniciar servidor php: ``php -S localhost:1234``
2. Iniciar servidor webpack: ``npm run dev``

## Explicações
Em `index.php` vai procurar o javascript de `public` dependendo da constante `ENV`. Pois em desenvolvimento é `localhost:4444` (pelo webpack dev server) e em produção é `localhost:1234` (arquivos "buildados" estáticos).

### OBS:
- `public` não é versionado

## Fluxo de trabalho
### Localmente (em ambiente dev)
- rodar ``npm run dev`` para ver alterações nos arquivos

### No servidor (em ambiente prod)
- rodar ``npm run build`` no script de deploy para gerar a pasta public

## TODO (configurações/exemplos básicas do setup)
- [ ] Especificar arquivos de entrada e saída
- [ ] Imports e exports
- [ ] Jquery, semantic e jquery mask
